
from dominio import IRepositorioVehiculo, Vehiculo


def inscribir_vehiculo(placa: str, motor: str, repositorio: IRepositorioVehiculo) -> None:
    nuevo_vehiculo = Vehiculo(placa, motor)
    repositorio.guardar_vehiculo(nuevo_vehiculo)

def cambio_placa(placa_vieja: str, placa_nueva: str, repositorio: IRepositorioVehiculo) -> None:
    vehiculo = repositorio.obtener_vehiculo(placa_vieja)

    if vehiculo is None:
        raise ValueError(f"No existe el vehiculo con placa {placa_vieja}")

    vehiculo.cambiar_placa(placa_nueva)
    repositorio.guardar_vehiculo(vehiculo)
