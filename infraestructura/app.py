
from fastapi import FastAPI
from aplicacion import inscribir_vehiculo, cambio_placa
from infraestructura.persistencia import RepositorioVehiculoEnMemoria


app = FastAPI()
repositorio = RepositorioVehiculoEnMemoria()


@app.post('/vehiculo')
async def post_vehiculo(placa: str, motor: str):
    try:
        inscribir_vehiculo(placa, motor, repositorio)
        return {"mensaje": "vehiculo inscrito"}
    except Exception as e:
        return {"error": str(e)}

@app.patch('/vehiculo')
async def patch_cambio_placa(placa_vieja: str, placa_nueva: str):
    try:
       cambio_placa(placa_vieja, placa_nueva, repositorio)
       return {"mensaje": "placa cambiada"}
    except Exception as e:
        return {"error": str(e)}
