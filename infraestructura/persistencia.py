
from dominio import IRepositorioVehiculo, Vehiculo


class RepositorioVehiculoEnMemoria(IRepositorioVehiculo):

    vehiculos: list[Vehiculo]

    def __init__(self) -> None:
        self.vehiculos = []

    def guardar_vehiculo(self, vehiculo: Vehiculo) -> None:
        self.vehiculos.append(vehiculo)

    def obtener_vehiculo(self, placa: str) -> Vehiculo | None:
        vehiculo_encontrado: Vehiculo | None = None
        for i, vehiculo in enumerate(self.vehiculos):
            if vehiculo.placa == placa:
                vehiculo_encontrado = self.vehiculos.pop(i)
        return vehiculo_encontrado
    
