EJEMPLO DE DDD CON PYTHON Y FASTAPI
===================================

Este archivo contiene un ejemplo de desarrollo orientado al dominio (DDD), arquitectura hexagonal y 
una api restful con FastAPI.

Instalación
-----------

Para instalar se necesita usa `pipenv` para poder manejar las dependencias y los entornos virtuales. Una
vez instalado podemos utilizar el siguiente comando sobre la carpeta:

~~~sh
pipenv install
~~~

Ejecutar Proyecto
-----------------

Para ejecutar el proyecto se necesita uvicorn (instalado con las dependencias en el comando anterior), para correrlo
utilice el siguiente comando:

~~~sh
pipenv run uvicorn persistencia.app:app
~~~

Desarrollo
----------

Para los desarrolladores se recomienda tener instalado las extensiones de Python y MyPy para el chequeo estático de los tipos.

Arquitectura
------------

La arquitectura que utiliza el proyecto es la **hexagonal**, por lo que se ve de la siguiente forma:

~~~plantuml
top to bottom direction
package dominio {
    class Vehiculo <<AgregadoRaiz>> {
        - placa: String
        - motor: String
        + cambiar_placa(placa: String): Void
    }
    interface IRepositorioVehiculo {
        + guardar_vehiculo(vehiculo: Vehiculo): Void
        + obtener_vehiculo(placa: String): Vehiculo
    }
    Vehiculo <-down- IRepositorioVehiculo
}
package aplicacion {
    class aplicacion <<ModuloPython>> {
        + inscribir_vehiculo(placa: String, motor: String, repositorio: IRepositorioVehiculo): Void
        + cambiar_placa(placa_vieja: String, placa_nueva: String, repositorio: IRepositorioVehiculo): Void        
    }
    IRepositorioVehiculo <-down- aplicacion
}

package infraestructura {
    package persistencia {
        class RepositorioVehiculoEnMemoria {
            - vehiculos: Array<Vehiculo>

        }
        IRepositorioVehiculo <|-- RepositorioVehiculoEnMemoria
    }
    class app <<ModuloPython>> {
        + post_vehiculo(placa: String, motor: String): Response
        + patch_cambio_placa(placa_vieja: String, placa_nueva: String): Response
    }

    app -up-> RepositorioVehiculoEnMemoria
}

app -up-> aplicacion.aplicacion
~~~

La forma en que viaja la información se puede ver en el siguiente diagrama:

![diagrama hexagonal](./diagrama.png)