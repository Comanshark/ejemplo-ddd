from typing import Protocol


class Vehiculo:

    __placa: str
    __motor: str

    @property
    def placa(self) -> str:
        return self.__placa

    @property
    def motor(self) -> str:
        return self.__motor
    
    def __init__(self, placa: str, motor: str) -> None:
        self.__placa = placa
        self.__motor = motor

    def cambiar_placa(self, placa_nueva: str) -> None:
        if placa_nueva == self.__placa:
            raise ValueError("No se puede aplicar la misma placa")
        
        self.__placa = placa_nueva


class IRepositorioVehiculo(Protocol):
    
    def guardar_vehiculo(self, vehiculo: Vehiculo) -> None:
        ...
    
    def obtener_vehiculo(self, placa: str) -> Vehiculo | None:
        ...
    